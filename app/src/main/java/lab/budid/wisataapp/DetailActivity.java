package lab.jonathansr.trywisata;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class DetailActivity extends AppCompatActivity {

    DatabaseHelper database = new DatabaseHelper(this);
    ArrayList<WisataModel> listData = new ArrayList<>();
    private static final String TAG = "DetailWisataActivity";
    private static final String TAG_PREF = "setting";
    private static final String TAG_FAV = "favorite";

    private ImageView ivDetailGambar;
    private TextView tvDetailDeskripsi;
    private TextView tvDetailAlamat;
    FloatingActionButton fab;
    private String namaWisata, gambarWisata, deskripsiWisata, alamatWisata, latWisata, longWisata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        initView();

        Bundle data = getIntent().getExtras();
        final String idWisata = data.getString(Konstanta.DATA_ID);
        namaWisata = data.getString(Konstanta.DATA_NAMA);
        gambarWisata = data.getString(Konstanta.DATA_GAMBAR);
        deskripsiWisata = data.getString(Konstanta.DATA_DESKRIPSI);
        alamatWisata = data.getString(Konstanta.DATA_ALAMAT);
        latWisata = data.getString(Konstanta.DATA_LAT);
        longWisata = data.getString(Konstanta.DATA_LNG);


        getSupportActionBar().setTitle(namaWisata);
        tvDetailDeskripsi.setText(deskripsiWisata);
        tvDetailAlamat.setText(alamatWisata);
        Glide.with(this)
                .load("http://192.168.43.127/wisata/img/wisata/" + gambarWisata)
                .placeholder(R.drawable.ic_menu_camera)
                .error(R.drawable.ic_menu_camera)
                .into(ivDetailGambar);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        cekFavorit(idWisata);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //dilakukan jika nilai sp favorite sudah ada nilainya, dan tombol favorite diklik lagi
                if (cekFavorit(idWisata)) {
                    //hapus data
                    database.delete(namaWisata);

                    Snackbar.make(view, "database berhasil dihapus", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    SharedPreferences sp = getSharedPreferences(TAG_PREF, MODE_PRIVATE);
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putBoolean(TAG_FAV + idWisata, false);
                    editor.commit();
                } else {
                    //insert
                    long id = database.insertData(namaWisata, gambarWisata, alamatWisata, deskripsiWisata, latWisata, longWisata);

                    if (id <= 0) {
                        Snackbar.make(view, "database gagal dimasuki data", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    } else {
                        Snackbar.make(view, "database berhasil dimasuki data", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();

                        SharedPreferences sp = getSharedPreferences(TAG_PREF, MODE_PRIVATE);
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putBoolean(TAG_FAV + idWisata, true);
                        editor.commit();

                        // fab.setImageResource(R.drawable.ic_action_isfavorit);
                    }
                }
                //memanggil cekFavorit saat fab diklik
                cekFavorit(idWisata);


            }
        });


    }

    private boolean cekFavorit(String id) {
        SharedPreferences sp = getSharedPreferences(TAG_PREF, MODE_PRIVATE);
        boolean isFav = sp.getBoolean(TAG_FAV + id, false);

        if (isFav) {
            fab.setImageResource(R.drawable.ic_favorite_no);

        } else {
            fab.setImageResource(R.drawable.ic_favorite);

        }
        return isFav;

    }
    private void initView() {
        ivDetailGambar = (ImageView) findViewById(R.id.iv_detail_gambar);
        tvDetailDeskripsi = (TextView) findViewById(R.id.tv_detail_deskripsi);
        tvDetailAlamat = (TextView) findViewById(R.id.tv_detail_alamat);
    }

}
