package lab.budid.wisataapp;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiServices {
    @GET("getwisata.php")
    Call<ListWisataModel> ambilDataWisata();

}
